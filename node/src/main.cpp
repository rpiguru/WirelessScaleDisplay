#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ADS1230.h>
#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include <EEPROM.h>


// ============================== Change values here ==============================
// Change this value for each WiFi devices - LF, LR, RF, RR
const char* device_name = "LF";

const char* ssid = "WSD-80df3dba";                                // RPi3's AP name
//const char* ssid = "WSD-4e0d53e0";                                // RPi3's AP name

// ============== ADS1230 Configuration ==================================
// Load cell 1: 5000lb capacity and 1.9986mV/V sensitivity on pin D4 (5000lb = 2267.962kg)
ADS1230 ads_wheel(2267962, 1.9986, D4);

// Load cell 2: 5000lb capacity and 1.9986mV/V sensitivity on pin D3 (5000lb = 2267.962kg)
ADS1230 ads_bs(2267962, 1.9986, D3);


#define UPLOAD_INTERVAL       1000                                 // Uploading interval of device state in miliseconds


//    --------  Constant values -----------
const char* password = "drp_products";                            // RPi3's AP password
const char* mqtt_server = "172.24.1.1";                           // Server has been built on the router(RPi 3) itself
//
const char* suffix_potentio = "/potentiometer/state";
const char* suffix_potentio_zero = "/potentiometer/zero";
const char* suffix_potentio_calibrate = "/potentiometer/calibrate";
//
const char* suffix_weight_wheel = "/weight_wheel/state";
const char* suffix_weight_wheel_zero = "/weight_wheel/zero";
const char* suffix_weight_wheel_calibrate = "/weight_wheel/calibrate";
//
const char* suffix_weight_bs = "/weight_bs/state";
const char* suffix_weight_bs_zero = "/weight_bs/zero";
const char* suffix_weight_bs_calibrate = "/weight_bs/calibrate";


Adafruit_ADS1115 ads1115;
uint16_t adc_val = 0;
uint16_t position_offset = 0;
float position_val = 0;

float weight_bs = 0;
float weight_wheel = 0;

#define MICRODELAY                      13                              // Read load cell data every 13ms (Frequency of ADS1230 is 80Hz)

#define ADS1015_AVERAGE_COUNT           10                              // Count value to get an average value

#define EEPROM_ADDR_OFFSET_BS           0
#define EEPROM_ADDR_OFFSET_WHEEL        3
#define EEPROM_ADDR_OFFSET_POS          6

WiFiClient espClient;
PubSubClient client(espClient);

String macAddr;
char buf_pub_topic[50];
char buf_sub_topic[50];
char* buf_mqtt = new char[10];

volatile unsigned long ContactBounceTime;                         // Timer to avoid contact bounce in interrupt routine
unsigned long s_time;


uint16_t readAverageADC(uint8_t channel);


void setup_wifi() {
  int attempt = 0;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}

void set_pub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix){
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++){
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}

void on_mqtt_message(char* topic, byte* payload, unsigned int length) {
  set_sub_topic(suffix_weight_wheel_calibrate);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    Serial.println("Calibrating the Weight Wheel sensor");Serial.println("");
    ads_wheel.calibrate();
  }
  set_sub_topic(suffix_weight_wheel_zero);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    ads_wheel.calibrate();
    int32_t val = ads_wheel.getRawValue();
    Serial.print("Setting Zero of the Weight Wheel sensor, Raw Val: ");Serial.println(val);
    Serial.print("Offset: "); Serial.println(ads_wheel.offset);
    EEPROM.write(EEPROM_ADDR_OFFSET_WHEEL, val / 65536);
    EEPROM.write(EEPROM_ADDR_OFFSET_WHEEL + 1, (val % 65536) / 256);
    EEPROM.write(EEPROM_ADDR_OFFSET_WHEEL + 2, val % 256);
    EEPROM.commit();
    ads_wheel.setZero();
  }

  set_sub_topic(suffix_weight_bs_calibrate);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    Serial.println("Calibrating the Bump Stop Load sensor");Serial.println("");
    ads_bs.calibrate();
  }
  set_sub_topic(suffix_weight_bs_zero);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    ads_bs.calibrate();
    int32_t val = ads_bs.getRawValue();
    Serial.print("Setting Zero of the Bump Stop Load sensor, Raw val: ");Serial.println(val);
    Serial.print("Offset: "); Serial.println(ads_bs.offset);
    EEPROM.write(EEPROM_ADDR_OFFSET_BS, val / 65536);
    EEPROM.write(EEPROM_ADDR_OFFSET_BS + 1, (val % 65536) / 256);
    EEPROM.write(EEPROM_ADDR_OFFSET_BS + 2, val % 256);
    EEPROM.commit();
    ads_bs.setZero();
  }

  set_sub_topic(suffix_potentio_calibrate);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    Serial.println("Calibrating the Potentiometer");Serial.println("");
    // TODO: Implement calibration logic here...
  }
  
  set_sub_topic(suffix_potentio_zero);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
    Serial.println("Setting Zero of the Potentiometer");Serial.println("");
    adc_val = readAverageADC(0);
    position_offset = adc_val;
    EEPROM.write(EEPROM_ADDR_OFFSET_POS, adc_val / 256);
    EEPROM.write(EEPROM_ADDR_OFFSET_POS + 1, adc_val % 256);
    EEPROM.commit();
  }  

}

void reconnect() {
  int attempt = 0;
  // Loop until we're reconnected
  while (!client.connected()) {
    if (attempt < 3) attempt ++;
    else ESP.restart();
    // Attempt to connect
    if (client.connect(macAddr.c_str())) {
      Serial.println("Connected to the MQTT server... ");
      
      set_sub_topic(suffix_weight_wheel_calibrate);
      client.subscribe(buf_sub_topic);
      set_sub_topic(suffix_weight_wheel_zero);
      client.subscribe(buf_sub_topic);
      
      set_sub_topic(suffix_weight_bs_calibrate);
      client.subscribe(buf_sub_topic);
      set_sub_topic(suffix_weight_bs_zero);
      client.subscribe(buf_sub_topic);

      set_sub_topic(suffix_potentio_calibrate);
      client.subscribe(buf_sub_topic);
      set_sub_topic(suffix_potentio_zero);
      client.subscribe(buf_sub_topic);

    } else {
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void ICACHE_RAM_ATTR onTimerISR(){
  ads_wheel.readDataIfReady();
  ads_bs.readDataIfReady();
}


void setup() {

  Serial.begin(9600);

  pinMode(D0, OUTPUT);

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(on_mqtt_message);

  ads_wheel.begin();
  ads_bs.begin();
  
  EEPROM.begin(512);
  uint32_t val = EEPROM.read(EEPROM_ADDR_OFFSET_WHEEL) * 65536 + EEPROM.read(EEPROM_ADDR_OFFSET_WHEEL + 1) * 256 + EEPROM.read(EEPROM_ADDR_OFFSET_WHEEL + 2);
  if (val < 256 * 256 * 256 - 1){   // If not 0xFFFFFF
    Serial.print("Recovered the OFFSET value of ADS_WHEEL - "); Serial.println(val);
    ads_wheel.offset = val;
  }
  
  val = EEPROM.read(EEPROM_ADDR_OFFSET_BS) * 65536 + EEPROM.read(EEPROM_ADDR_OFFSET_BS + 1) * 256 + EEPROM.read(EEPROM_ADDR_OFFSET_BS + 2);
  if (val < 256 * 256 * 256 - 1){   // If not 0xFFFFFF
    Serial.print("Recovered the OFFSET value of ADS_BS - "); Serial.println(val);
    ads_bs.offset = val;
  }

  val = EEPROM.read(EEPROM_ADDR_OFFSET_POS) * 256 + EEPROM.read(EEPROM_ADDR_OFFSET_POS + 1);
  if (val < 256 * 256 - 1) {        // If not 0xFFFF
    Serial.print("Recovered the OFFSET value of POSITIO - "); Serial.println(val);
    position_offset = val;
  }

  // HW Timer 0 is used by WiFi Functions, and only Timer1 can be used.
  timer1_disable();
  timer1_attachInterrupt(onTimerISR);
  timer1_isr_init();
  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);     // 5MHz (5 ticks/us - 1677721.4 us max)
  timer1_write(clockCyclesPerMicrosecond() * MICRODELAY * 1111);

  ads1115.begin();
  ads1115.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 0.125mV

  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  s_time = millis();
  // publish potentiometer state
  adc_val = readAverageADC(0);
  // adc_val = adc_val > position_offset ? adc_val - position_offset : 0;
  int16_t calibrated_val;
  calibrated_val = (int16_t)adc_val - (int16_t)position_offset;
  position_val = (float)calibrated_val * 4.096 / 65536.0 * 2.0 * 1000;
  // position_val = (float)analogRead(A0) / 1024 * 3300;
  Serial.print("Potentiometer: ");  Serial.print(position_val); Serial.println("mV");
  sprintf(buf_mqtt, "%.3f", position_val);
  set_pub_topic(suffix_potentio);
  client.publish(buf_pub_topic, buf_mqtt);

  if (ads_wheel.updated()){
    weight_wheel = ads_wheel.getWeight();
    Serial.print("Weight Wheel: "); Serial.println(weight_wheel);
    sprintf(buf_mqtt, "%.3f", weight_wheel);
    set_pub_topic(suffix_weight_wheel);
    client.publish(buf_pub_topic, buf_mqtt);
  }

  if (ads_bs.updated()){
    weight_bs = ads_bs.getWeight();
    Serial.print("Weight Bump Stop: "); Serial.println(weight_bs);
    sprintf(buf_mqtt, "%.3f", weight_bs);
    set_pub_topic(suffix_weight_bs);
    client.publish(buf_pub_topic, buf_mqtt);
  }
  int elapsed = millis() - s_time;
  if (elapsed < UPLOAD_INTERVAL){
    delay(UPLOAD_INTERVAL - elapsed);
  }
}

uint16_t readAverageADC(uint8_t channel){
  uint16_t val[ADS1015_AVERAGE_COUNT];
  for (int i = 0; i < ADS1015_AVERAGE_COUNT; i++){
    val[i] = ads1115.readADC_SingleEnded(channel);
  }
  uint16_t max_val = val[0];
  uint16_t min_val = val[0];
  for (int i = 1; i < ADS1015_AVERAGE_COUNT; i++){
    if (max_val < val[i]) max_val = val[i];
    if (min_val > val[i]) min_val = val[i];
  }
  long buf = 0, buf_max = 0, buf_min = 0;
  uint16_t cnt = 0, cnt_max = 0, cnt_min = 0;
  for (int i = 0; i < ADS1015_AVERAGE_COUNT; i++){
    if (val[i] == min_val){
      buf_min += val[i];
      cnt_min ++;
    } else if (val[i] == max_val){
      buf_max += val[i];
      cnt_max ++;
    } else {
      buf += val[i];
      cnt ++;
    }
  }
  if (cnt == 0){
    if (cnt_min > cnt_max)
      return min_val;
    else if (cnt_max > cnt_min)
      return max_val;
    else
      return (max_val + min_val) / 2;
  }
  else
    return buf / cnt;
}