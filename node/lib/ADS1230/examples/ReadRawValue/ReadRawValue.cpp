/**
 * Read raw 20-bit integer value from a load cell using the ADS1230 IC.
 *
 */
#include <SPI.h>
#include "ADS1230.h"

//  - Load cell capacity: 100kg
//  - Load cell sensitivity: 3mV/V
//  - CS on pin D8 (D8 jumper closed)
//  - High gain (GAIN jumper closed)
//  - No averaging (number of samples = 1)
ADS1230 loadCell(100000, 3, 8, true, 1);

void setup() {
  Serial.begin(9600);
  loadCell.begin();
}

void loop() {
  if (loadCell.updated()) {
    Serial.println(loadCell.getLatestRawValue());
  }
}
