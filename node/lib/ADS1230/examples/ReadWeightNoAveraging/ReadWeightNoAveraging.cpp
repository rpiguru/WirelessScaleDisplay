/**
 * Read weight from a load cell using the ADS1230 IC, without averaging.
 *
 */

#include <SPI.h>
#include "ADS1230.h"

//  - Load cell capacity: 100kg
//  - Load cell sensitivity: 3mV/V
//  - CS on pin D8 (D8 jumper closed)
//  - High gain (GAIN jumper closed)
//  - No averaging (number of samples = 1)
ADS1230 ads(100000, 3, 8, true, 1);

void setup() {
  Serial.begin(9600);
  ads.begin();
  
  // Wait for calibration and set current value to zero weight (tare)
  while (!ads.updated());
  ads.setZero();
}

void loop() {
  if (ads.updated()) {
    Serial.print(ads.getWeight(), 0);
    Serial.println("g");
  }
}
