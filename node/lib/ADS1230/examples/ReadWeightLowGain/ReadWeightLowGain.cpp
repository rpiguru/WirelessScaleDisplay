/**
 * Read weight from a load cell using the ADS1230 IC, with low gain.
 *
 * A low gain can be used when measuring weight over a wide range. To use a low gain the GAIN jumper
 *  must remain open. This example uses averaging of 10 samples.
 */
#include <SPI.h>
#include "ADS1230.h"

//  - Load cell capacity: 100kg
//  - Load cell sensitivity: 3mV/V
//  - CS on pin D8 (D8 jumper closed)
//  - Low gain (GAIN jumper open)
ADS1230 loadCell(100000, 3, 8, false);

void setup() {
  Serial.begin(9600);
  loadCell.begin();
  
  // Wait for calibration and set current value to zero weight (tare)
  while (!loadCell.updated());
  loadCell.setZero();
}

void loop() {
  if (loadCell.updated()) {
    Serial.print(loadCell.getWeight(), 0);
    Serial.println("g");
  }
}
