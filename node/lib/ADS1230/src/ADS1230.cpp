/** @file
 * This is an NodeMCU library to measure weight using load cells with the ADS1230 IC from Texas Instruments.
 *
*/

#include "ADS1230.h"


SPISettings ADS1230::spiSettings = SPISettings(4000000, MSBFIRST, SPI_MODE1);
int ADS1230::timeout = 120;


ADS1230::ADS1230(float capacity, float sensitivity, int cs, bool hiGain, int numSamples) {
  // Initialize fixed parameters
  this->cs = cs;
  this->offset = 0;
  this->capacity = capacity;
  this->sensitivity = sensitivity;
  this->hiGain = hiGain;
  this->calibrateOnNextCycle = false;
  
  if (numSamples > ADS1230_MAX_SAMPLES) {
    this->numSamples = ADS1230_MAX_SAMPLES;
  } else if (numSamples < 1) {
    this->numSamples = 1;
  } else {
    this->numSamples = numSamples;
  }
  
  // Reset circular buffer
  resetBuffer();

}

void ADS1230::begin(bool calibrate) {
  // Calibrate on first cycle
  calibrateOnNextCycle = calibrate;

  // Reset circular buffer
  resetBuffer();

  // Initialize hardware resources
  pinMode(cs, OUTPUT);
  digitalWrite(cs, HIGH);
  SPI.begin();
  // SPI.usingInterrupt(255);

}

bool ADS1230::updated() {
  bool nd;
  uint8_t as;
  noInterrupts();
  nd = newData;
  as = actualSamples;
  interrupts();
  return nd && as >= numSamples;
}

int32_t ADS1230::getRawValue() {
  long buf = 0, buf_max = 0, buf_min = 0;
  uint8_t cnt = 0, cnt_max = 0, cnt_min = 0;

  noInterrupts();
  newData = false;
  // Get MAX & MIN value
  int32_t max_val = samples[0];
  int32_t min_val = samples[0];
  for (int i = 1; i < actualSamples; i++){
    if (max_val < samples[i]) max_val = samples[i];
    if (min_val > samples[i]) min_val = samples[i];
  }
  // Remove MAX & MIN value, and get an average value
  for (int i = 0; i < actualSamples; i++){
    if (samples[i] == min_val){
      buf_min += samples[i];
      cnt_min ++;
    } else if (samples[i] == max_val){
      buf_max += samples[i];
      cnt_max ++;
    } else {
      buf += samples[i];
      cnt ++;
    }
  }
  interrupts();

  if (cnt == 0)
    if (cnt_min > cnt_max)
      return min_val;
    else if (cnt_max > cnt_min)
      return max_val;
    else
      return (max_val + min_val) / 2;
  else
    return buf / cnt;
}

int32_t ADS1230::getValue() {
  int32_t ss;
  ss = getRawValue();
  return ss - offset;
}

int32_t ADS1230::getLatestValue() {
  int32_t st;
  int32_t of;
  noInterrupts();
  newData = false;
  st = samples[tail];
  of = offset;
  interrupts();
  return st - of;
}

int32_t ADS1230::getLatestRawValue() {
  int32_t st;
  noInterrupts();
  newData = false;
  st = samples[tail];
  interrupts();
  return st;
}

float ADS1230::getWeight() {
  return capacity * getValue() / ((1L << 20) * (hiGain ? 128 : 64) * (sensitivity / 1000));
}

void ADS1230::setZero() {
  offset = getRawValue();
}

void ADS1230::calibrate() {
  noInterrupts();
  calibrateOnNextCycle = true;
  interrupts();
}

void ADS1230::readDataIfReady() {
  // Check if there is new data to read
  SPI.beginTransaction(spiSettings);
  digitalWrite(cs, LOW);
  if (digitalRead(MISO) == LOW) {
    // Read data via SPI if /DRDY is low
    int32_t sample = 0;
    sample |= SPI.transfer(0);
    sample <<= 8;
    sample |= SPI.transfer(0);
    sample <<= 8;
    sample |= SPI.transfer(0);
    sample <<= 8;
    sample /= 1L << 12;
    sample = sample > 0 ? sample : 0;

    if (calibrateOnNextCycle) {
      // Send at least two more clock pulses to initiate ADS1230 offset calibration, if requested
      SPI.transfer(0);
      calibrateOnNextCycle = false;
    } else {
      // Save sample in circular buffer
      if (++actualSamples > numSamples) {
        // Buffer full, remove oldest sample (head), removing it from the sum
        actualSamples = numSamples;
        samplesSum -= samples[head];
        if (++head >= numSamples) {
          head = 0;
        }
      }
      
      // Put new sample in the circular buffer tail and add it to the sum
      if (++tail >= numSamples) {
        tail = 0;
      }
      samples[tail] = sample;
      samplesSum += sample;
      
      // Inform there is new data
      newData = true;
    }
  }
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
}

void ADS1230::resetBuffer() {
  noInterrupts();
  newData = false;
  actualSamples = 0;
  samplesSum = 0;
  head = 0;
  tail = numSamples - 1;
  samples[tail] = 0;
  interrupts();
}
