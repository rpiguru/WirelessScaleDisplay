# Wireless Scale Device Node

## Configuring NanoShield Board.

- As the NodeMCU has a 3V3 power only, we need to modify the NanoShield Load Cell Module. 

    Please follow this - https://www.circuitar.com/nanoshields/modules/load-cell/#33v-operation

- Keep the **GAIN** jumper connected to use higher gain(128).
    
- Open the **SPEED** jumper to read 80 samples per second.

## Connection Guide

1. Connecting the load cell to the Nanoshield module.
    
    See the **Connecting the load cell** section of [this](https://www.circuitar.com/nanoshields/modules/load-cell/) link.

2. Connecting the Nanoshild module to the NodeMCU.
    
    ![Breadboard View](schematic/node_bb.jpg)

    ![Schematic View](schematic/node_schem.jpg)

**IMPORTANT NOTES IN THE PICTURE ABOVE:**

- Instead of the **NanoShield** module, I used *BME280* breakout board which has the same pin layout.

- The default **CS** pin of the Nanoshield module is **D8**, so you should connect D3/D4 pin of NodeMCU to D8 of each NanoShield module.

![NodeMCU and Nanoshield module](../doc/photos/node_connection_1.jpg)

![Pins of the NodeMCU](../doc/photos/node_connection_NodeMCU.jpg)

![Pins of the Nanoshield](../doc/photos/node_connection_Nanoshield.jpg)

  
## Programming the NodeMCU

1. Installing Visual Studio Code. (Skip this if you had already installed on your PC)

- Download and install Visual Studio Code from [here](https://code.visualstudio.com/download)

- Install **PlatformIO IDE** by following [this](http://docs.platformio.org/en/latest/ide/vscode.html#installation) link.

2. Download the source code from the gitlab repo.

- Visit https://gitlab.com/rpiguru/WirelessScaleDisplay

- Download the source code in zip format and extract all files to your hard drive.

3. Install the USB driver of the NodeMCU. (Skip this if you had already installed the driver on your PC)

    In default, Windows does not support the USB driver of the NodeMCU.
    
    Follow this link - https://cityos-air.readme.io/docs/1-usb-drivers-for-nodemcu-v10#section-13-nodemcu-v10-driver

4. Programme the NodeMCU.

- Open the **VS Code**.

- `File` -> `Open Folder`, and select the `node` directory of our repository.

- After opening successfully, you should see following:
    
    ![VS Code](../doc/photos/vscode.png)

- Open `src/main.cpp` by double clicking it, and change some values in the source code.
    
    * `device_name`(line 10): Use `LF`/`LR`/`RF`/`RR`
    * `ssid`(line 12): Find the correct SSID of your RPi and write it.
    
        How to find the SSID of your RPi?
         
        After executing the installation script on RPi, on your mobile you should be able to see a WiFi AP named `WSD-[sn]`.
                
    * If you are using the different load cell, check line 16~20

- Press `Alt+Ctrl+B` to build the source code.

- Press `Alt+Ctrl+U` to upload to the NodeMCU.

- Press `Alt+Ctrl+S` to open the serial monitor and see how it works.
    
    *NOTE:* NodeMCU's blue LED will blink twice in a second when it tried to connect to the RPi thru wifi.
