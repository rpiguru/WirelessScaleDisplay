# Wireless Scale Display

## Components

- Raspberry Pi 3
    
    https://www.raspberrypi.org/products/raspberry-pi-3-model-b/  

- 7" Touchscreen from WaveShare
    
    https://www.waveshare.com/product/7inch-hdmi-lcd.htm

- NodeMCU
 
    https://www.amazon.com/NodeMCU-ESP8266-ESP-12E-Development-Board/dp/B0741WPPDY/ref=sr_1_3?s=electronics&ie=UTF8&qid=1517534541&sr=1-3&keywords=nodemcu

- Load Cell Nanoshield - ADS1230 Load Cell Module
    
    https://www.tindie.com/products/Circuitar/load-cell-nanoshield-ads1230-load-cell-module/


## Installation

- [Display Setup](display/README.md)

- [Node Setup](node/README.md)
 