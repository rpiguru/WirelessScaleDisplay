#!/usr/bin/env bash

sudo apt-get install -y libssl-dev
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo dpkg -i ${cur_dir}/libssl1.0.0_1.0.1t-1+deb8u9_armhf.deb

echo "Creating mongodb user..."
adduser --ingroup nogroup --shell /etc/false --disabled-password --gecos "" --no-create-home mongodb

echo "Downloading mongodb compiled binaries... "
cd ~
wget http://andyfelong.com/downloads/core_mongodb_3_0_14.tar.gz
tar zxvf core_mongodb_3_0_14.tar.gz

chown root:root mongo*
chmod 755 mongo*
strip mongo*
cp -p mongo* /usr/bin
rm mongo*

echo "Create log file directory with appropriate owner & permissions"
mkdir /var/log/mongodb
chown mongodb:nogroup /var/log/mongodb

echo "Create the DB data directory with convenient access perms"
mkdir /var/lib/mongodb
chown mongodb:root /var/lib/mongodb
chmod 775 /var/lib/mongodb

echo "Create the mongodb.conf file in /etc"
sh -c "cat > /etc/mongodb.conf << EOF
# /etc/mongodb.conf
# minimal config file (old style)
# Run mongod --help to see a list of options

bind_ip = 127.0.0.1
quiet = true
dbpath = /var/lib/mongodb
logpath = /var/log/mongodb/mongod.log
logappend = true
storageEngine = mmapv1
EOF"

echo "Create systemd/service entry"
sh -c "cat > /lib/systemd/system/mongodb.service << EOF
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongodb.conf

[Install]
WantedBy=multi-user.target

EOF"

export LC_ALL=C

sudo systemctl enable mongodb

echo "Starting mongodb service..."
service mongodb start
