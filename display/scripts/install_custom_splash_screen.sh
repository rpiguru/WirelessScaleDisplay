#!/usr/bin/env bash

# Remove `Raspbian GNU/Linux 8 raspberrypi tty1` message
sudo rm /etc/issue
sudo touch /etc/issue

# Remove login prompt
sudo systemctl disable getty@tty1.service

sudo apt-get install -y omxplayer

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
sudo cp ${cur_dir}/drp_splash.mp4 /opt/splash.mp4

sudo cp ${cur_dir}/splashscreen.service /etc/systemd/system/splashscreen.service
sudo systemctl enable splashscreen
