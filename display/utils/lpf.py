import numpy as np
from scipy.signal import butter, lfilter
import matplotlib.pyplot as plt
from settings import LPF_ORDER, LPF_FS, LPF_CUTOFF


class LPF:

    def __init__(self, order=LPF_ORDER, fs=LPF_FS, cutoff=LPF_CUTOFF):
        self.fs = fs
        self.cutoff = cutoff
        self.order = order
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        self.b, self.a = butter(N=order, Wn=normal_cutoff, btype='low', analog=False)
        self.reg = [0] * order * 3

    def filter(self, x):
        self.reg = self.reg[1:] + [x]
        y = lfilter(self.b, self.a, self.reg)
        return y[-1]


if __name__ == '__main__':

    f = LPF()

    # Demonstrate the use of the filter.
    # First make some data to be filtered.
    T = 5.0         # seconds
    n = int(T * f.fs)  # total number of samples
    t = np.linspace(0, T, n, endpoint=False)
    # "Noisy" data.  We want to recover the 2 Hz signal from this.
    data = np.sin(2 * 2 * np.pi * t) + 1.5 * np.cos(15 * 2 * np.pi * t) + 0.5 * np.sin(20 * 2 * np.pi * t)

    filtered = []
    for d in data:
        filtered.append(f.filter(d))

    plt.subplot(2, 1, 2)
    plt.plot(t, data, 'b-', label='data')
    plt.plot(t, filtered, 'g-', linewidth=2, label='filtered data')
    plt.xlabel('Time [sec]')
    plt.grid()
    plt.legend()

    plt.subplots_adjust(hspace=0.35)
    plt.show()
