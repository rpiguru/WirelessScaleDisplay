# Wireless Display with Raspberry Pi

## Installation

- Download the latest Raspbian **Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/)
and flash your MicroSD card.

- Clone this repo
 
 
        cd ~
        git clone https://gitlab.com/rpiguru/WirelessScaleDisplay 

- Install everything:
 
       
        cd display
        bash setup.sh

  After the installation, you will be able to see a new WiFi AP named `WSD-[sn]`.
  (`sn` is the unique serial number of RPi)

  The GUI application will be started automatically at the next booting time.

  *NOTE*: Password of this AP is `drp_products`
