import datetime
import traceback
import xlsxwriter
import threading
from kivy.uix.boxlayout import BoxLayout
from kivy.utils import get_color_from_hex as rgb
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty
from kivy.logger import Logger
from screens.base import BaseScreen
from settings import DEVICES, DEVICES_LONG, INTERVAL
from utils.db import get_sensor_data, drop_db, get_raw_sensor_data
from utils.usb import get_connected_usb_device, save_file_to_usb_drive
from widgets.dialog import YesNoDialog
from scipy.interpolate import interp1d
import numpy as np
from widgets.graph import Graph, SmoothLinePlot
from widgets.label import H4
from widgets.snackbar import Snackbar


Builder.load_file('screens/chart_screen.kv')


graph_theme = {
    'label_options': {
        'color': rgb('444444'),             # color of tick labels and titles
        'bold': True
    },
    'tick_color': rgb('A0A0A0'),            # ticks and grid
    'border_color': rgb('808080')}          # border drawn around each graph


class ChartScreen(BaseScreen):

    cur_wheel = StringProperty('LF')
    cur_dev_type = StringProperty('weight_wheel')

    _clk_chart = ObjectProperty(allownone=True)

    def on_enter(self, *args):
        super(ChartScreen, self).on_enter(*args)
        self.ids.btn_play.icon = 'pause' if self.app.is_recording else 'play'
        if self.app.is_recording:
            self._clk_chart = Clock.schedule_interval(lambda dt: self._update_chart(), INTERVAL)
            self._update_chart()

    def on_wheel_changed(self):
        self.cur_wheel = DEVICES[DEVICES_LONG.index(self.ids.current_wheel.get_value())]
        if self.app.is_recording:
            self._update_chart()

    def on_dev_type_changed(self):
        self.cur_dev_type = 'weight_wheel' if self.ids.dev_type.get_value() == 'Wheel Weight' else 'weight_bs'
        if self.app.is_recording:
            self._update_chart()

    def _update_chart(self):
        threading.Thread(target=self._draw_chart()).start()

    def _draw_chart(self):
        data = get_sensor_data(self.cur_wheel)
        if len(data) > 50:  # Show only last 50 elements
            data = data[-50:-1]
        data = sorted(data, key=lambda v: v['potentiometer'])
        x_values = [d['potentiometer'] for d in data]
        y_values = [d[self.cur_dev_type] for d in data]
        if len(x_values) > 1:
            w, h = self.ids.box.size
            p = self.ids.box.padding[0]
            layout = BoxLayout(orientation='vertical', opacity=0, size_hint=(None, None), size=(w - 2 * p, h - 2 * p))
            try:
                f = interp1d(x_values, y_values, kind='linear')
                x_new = np.arange(min(x_values), max(x_values), (max(x_values) - min(x_values)) / float(len(x_values)))
                x_new = [min(x, float(max(x_values))) for x in x_new]
                x_new = [max(x, float(min(x_values))) for x in x_new]
                y_new = f(x_new)
                graph = Graph(
                    xlabel='Travel',
                    ylabel='Wheel Weight' if self.cur_dev_type == 'weight_wheel' else 'Bump Load',
                    x_ticks_major=(max(x_new) - min(x_new)) / 5.,
                    x_ticks_minor=5,
                    y_ticks_major=(max(y_new) - min(y_new)) / 5.,
                    y_ticks_minor=5,
                    y_grid_label=True,
                    x_grid_label=True,
                    padding=5,
                    xlog=False,
                    ylog=False,
                    x_grid=True,
                    y_grid=True,
                    x_unit='in',
                    xmin=float(min(x_new)),
                    xmax=float(max(x_new) if max(x_new) != min(x_new) else min(x_new) + 1),
                    ymin=float(min(y_new)) if len(y_new) > 0 else 0,
                    ymax=float(max(y_new) if max(y_new) != min(y_new) else min(y_new) + 1),
                    _with_stencilbuffer=False,
                    font_size='12sp',
                    precision='%.1f',
                    **graph_theme)

                plot = SmoothLinePlot(color=rgb('FF0000' if self.cur_dev_type == 'weight_wheel' else '0000FF'))
                plot.points = zip(x_new, y_new)
                graph.add_plot(plot)
                layout.add_widget(graph)
            except Exception as e:
                Logger.exception('Failed to draw the chart - {}'.format(e))
                self.app.save_exception(traceback.format_exc(limit=20))
                self.app.switch_screen('error_screen')
        else:
            layout = None
        Clock.schedule_once(lambda dt: self._add_chart_widget(layout))

    @mainthread
    def _add_chart_widget(self, layout):
        self.ids.box.clear_widgets()
        if layout:
            self.ids.box.add_widget(layout)
            Clock.schedule_once(lambda dt: self.show_layout(layout), .1)
        else:
            self.ids.box.add_widget(H4(text='Chart Data Not Available', font_style='Display1', halign='center'))

    @staticmethod
    def show_layout(layout):
        layout.opacity = 1

    def on_btn_download(self):
        dev_path = get_connected_usb_device()
        if dev_path is None:
            Snackbar(text="   Cannot find any USB Drive!   ", background_color=(.8, 0, .3, .5)).show()
            return
        threading.Thread(target=self._save_data_to_usb, args=(dev_path, )).start()

    def _save_data_to_usb(self, dev_path):
        # Create a xlsx file and export data.
        file_name = 'WSD_Export_{}.xlsx'.format(datetime.datetime.now().isoformat().replace(':', '-').split('.')[0])
        file_path = '/tmp/' + file_name
        workbook = xlsxwriter.Workbook(file_path)
        for wheel in DEVICES:
            data = get_raw_sensor_data(wheel)
            worksheet = workbook.add_worksheet(wheel)
            worksheet.write(0, 0, 'DateTime')
            worksheet.write(0, 1, 'Wheel Weight')
            worksheet.write(0, 2, 'Bump Load')
            worksheet.write(0, 3, 'Travel')
            for i, d in enumerate(data):
                worksheet.write(i + 1, 0, datetime.datetime.fromtimestamp(d['ts']).isoformat())
                worksheet.write(i + 1, 1, d['weight_wheel'])
                worksheet.write(i + 1, 2, d['weight_bs'])
                worksheet.write(i + 1, 3, d['potentiometer'])
        workbook.close()

        result = save_file_to_usb_drive(dev_path, file_path)

        Clock.schedule_once(lambda dt: self._show_result(result, file_name))

    @staticmethod
    def _show_result(result, file_name):
        if result:
            Clock.schedule_once(lambda dt: Snackbar(text="Successfully exported - {}".format(file_name), ).show())
        else:
            Clock.schedule_once(lambda dt: Snackbar(text="Failed to export", background_color=(.8, 0, .3, .5)).show())

    def on_btn_clear(self):
        dlg = YesNoDialog(message='Are you sure to clear all data?', title='Wireless Scale Display')
        dlg.bind(on_confirm=self.on_confirm_clear)
        dlg.open()

    def on_confirm_clear(self, *args):
        self.ids.box.clear_widgets()
        threading.Thread(target=drop_db).start()
        Snackbar(text="Cleared all data").show()
        args[0].dismiss()

    def on_btn_play(self, btn):
        if btn.icon == 'pause':
            btn.icon = 'play'
            self.app.stop_recording()
            if self._clk_chart:
                self._clk_chart.cancel()
                self._clk_chart = None
            Snackbar(text='Stopped Recording').show()
        else:
            btn.icon = 'pause'
            self.app.start_recording()
            self._clk_chart = Clock.schedule_interval(lambda dt: self._update_chart(), INTERVAL)
            self._update_chart()
            Snackbar(text='Started Recording').show()
