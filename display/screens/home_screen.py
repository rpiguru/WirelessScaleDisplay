from kivy.lang import Builder
from kivy.logger import Logger
from screens.base import BaseScreen
from settings import ADMIN_PASSWORD, DEBUG, DEVICES
from utils.common import is_rpi, get_config
from utils.constant import MQTT_ZERO
from widgets.dialog import PasswordDialog


Builder.load_file('screens/home_screen.kv')


class HomeScreen(BaseScreen):

    show_back_button = False

    def on_pre_enter(self, *args):
        super(HomeScreen, self).on_pre_enter(*args)
        if not get_config()['chart']:
            self.ids.box_bottom.remove_widget(self.ids.btn_chart)

    def on_btn_weights(self):
        self.switch_screen('weight_screen')

    def on_logo_long_pressed(self):
        if is_rpi() or not DEBUG:
            dlg = PasswordDialog(pwd=ADMIN_PASSWORD)
            dlg.bind(on_success=self.on_pwd_correct)
            dlg.open()
        else:
            self.on_pwd_correct()

    def on_pwd_correct(self, *args):
        self.switch_screen('calibration_screen')

    def on_btn_chart(self):
        self.switch_screen('chart_screen')

    def on_btn_mass_center(self):
        self.switch_screen('center_of_mass_screen')

    def on_btn_settings(self):
        self.switch_screen('settings_screen')

    def on_btn_zero_weights(self):
        Logger.info('WSD: Setting zero offset of all weights.')
        for dev in DEVICES:
            for _type in ['weight_wheel', 'weight_bs', ]:
                self.app.publish_mqtt_message(topic=MQTT_ZERO.format(device=dev, type=_type), payload='START')

    def on_btn_zero_travel(self):
        Logger.info('WSD: Setting zero offset of all potentiometers.')
        for dev in DEVICES:
            self.app.publish_mqtt_message(topic=MQTT_ZERO.format(device=dev, type='potentiometer'), payload='START')
